#!/bin/bash

# change user
#su - interminer << EOF
date=`date --iso-8601`

# Convert Db-Name to lower case
db_name=$(echo $DB_NAME | tr '[:upper:]' '[:lower:]')

echo $db_name

cd /data/intermine
cp ./bio/tutorial/project.xml $db_name/project.xml
sed -i 's/DATA_DIR/\/data/g' $db_name/project.xml

# populate-child-features post process is needed for JBrowse
xmlstarlet ed -O --inplace -s //post-processing -t elem -n post-process -v "" -i "(//post-processing/post-process)[last()]" -t attr -n name -v populate-child-features $db_name/project.xml

# add report displayer for JBrowse
xmlstarlet ed -O --inplace -s //reportdisplayers -t elem -n reportdisplayer -v "" \
    -i "(//reportdisplayers/reportdisplayer)[last()]" -t attr -n javaClass -v "org.intermine.bio.web.displayer.JBrowseDisplayer" \
    -i "(//reportdisplayers/reportdisplayer)[last()]" -t attr -n jspName -v "model/jbrowseDisplayer.jsp" \
    -i "(//reportdisplayers/reportdisplayer)[last()]" -t attr -n replacesFields -v "" \
    -i "(//reportdisplayers/reportdisplayer)[last()]" -t attr -n placement -v "Genomics" \
    -i "(//reportdisplayers/reportdisplayer)[last()]" -t attr -n types -v "SequenceFeature" \
    $db_name/webapp/resources/webapp/WEB-INF/webconfig-model.xml

echo "jbrowse.install.url = http://localhost:$JBROWSE_PORT" >> $db_name/webapp/resources/web.properties

# copy malaria data
cp ./bio/tutorial/malaria-data.tar.gz /data
cd /data
tar xvf malaria-data.tar.gz

until psql -h "postgres" -U "postgres" -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 2
done

# build malariamine
cd /data/intermine/$db_name/dbmodel
ant clean build-db
cd ../integrate
ant -Dsource=uniprot-malaria #-v
ant -Dsource=malaria-gff #-v
ant -Dsource=malaria-chromosome-fasta #-v
ant -Dsource=entrez-organism #-v
ant -Dsource=update-publications #-v
cd ../postprocess
ant #-v
cd ../webapp
ant build-db-userprofile
ant default remove-webapp release-webapp

# create refSeqs.json File
mkdir /jbrowse/seq
cd /jbrowse/seq
getRefSeqs.py "http://webapp:8080/$db_name" "$TAXON_ID"

# create psql dump
#echo "Create SQL-Dump in intermine-psql-dump/$DB_NAME.$date.dump"
#mkdir /data/intermine-psql-dump/
#cd /data/intermine/malariamine/
#../bio/scripts/project_build -b -v localhost /data/intermine-psql-dump/$DB_NAME.$date.dump
#cd /data/intermine-psql-dump/
#ln -s $DB_NAME.$date.dump.final latest.dump

# Stop Tomcat
#/opt/tomcat/bin/catalina.sh stop
echo "Exit bootstrap.sh"
