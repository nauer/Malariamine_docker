# Working but still under development

# Fork of zorino/docker-intermine https://github.com/zorino/docker-intermine/

# Intermine on Docker

Docker-compose project to build the demo Malariamine in few minutes. This project is meant to be a starting point for developing new Mines. Somebody can easily change and test different versions of the Intermine or connected software components like the Postgres database by simply changing the version number in the config file.

## Changes to the Standard Malariamine
* embed and view a JBrowse instance on the gene site

## Getting Started

### Prerequisities

docker and docker-compose

### Install malariamine with embedded JBrowse

This will build the Malariamine demo project from 3 containers.

```
git clone git@gitlab.com:nauer/Malariamine_docker.git
cd Malariamine_docker

# Initialize and start Container
docker-compose up

# Initialize Database
docker-compose exec webapp init_mine.sh

# Make the Malariamine Build
docker-compose exec webapp bootstrap-intermine-demo.sh

# Open the Malariamine in the Internet Browser
xdg-open http://localhost:8080/malariamine
```

### Configure
The build is configured by the single file `.env`. Make your changes there before building the mine.
```
# Postgres Settings
PSQL_DB_NAME=malariamine
PSQL_HOST_PORT=6000
PSQL_USER=interminer
PSQL_PWD=strong_pwd

# Tomcat Settings
TOMCAT_USER=tomcat
TOMCAT_PWD=strong_pwd

# JBrowse Settings
JBROWSE_PORT=9001
JBROWSE_DATA=jbrowse

# Intermine Database Settings
DB_NAME=malariamine

# Used Intermine Version
INTERMINE_VERSION=intermine-1.7.1

# Taxon Id
TAXON_ID=36329
```
