#!/bin/bash

date=`date --iso-8601`


# Convert Db-Name to lower case
db_name=$(echo $DB_NAME | tr '[:upper:]' '[:lower:]')

# Download intermine
cd /data

echo "Download or reset intermine.."
if [ ! -f $INTERMINE_VERSION.zip ]; then
    # Remove old database and dowload specific version (much smaller and faster than git clone)
    curl -LO https://github.com/intermine/intermine/archive/$INTERMINE_VERSION.zip
    unzip $INTERMINE_VERSION.zip
    ln -s  $(ls -d */) intermine
    cd intermine
else
    cd intermine
    # Reset mine / delete previous mine
    rm -r $db_name
fi

# Reset .intermine
rm -r /data/.intermine

# Create Mine
echo "Creating $DB_NAME folder structure.."
./bio/scripts/make_mine $DB_NAME

# Setup config and postgres
echo "Creating .intermine folder and set user credentials"

mkdir -p /data/.intermine
cp /data/intermine/bio/tutorial/malariamine.properties /data/.intermine/

sed -i "s/=localhost/=postgres/g" /data/.intermine/malariamine.properties
sed -i "s/PSQL_USER/$PSQL_USER/g" /data/.intermine/malariamine.properties
sed -i "s/PSQL_PWD/$PSQL_PWD/g" /data/.intermine/malariamine.properties
sed -i "s/TOMCAT_USER/$TOMCAT_USER/g" /data/.intermine/malariamine.properties
sed -i "s/TOMCAT_PWD/$TOMCAT_PWD/g" /data/.intermine/malariamine.properties

# Make .intermine from ~ available | make links for all users...
cd /root/
ln -s /data/.intermine .

# Wait for database
until psql -h "postgres" -U "postgres" -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 2
done

# Delete Databases if exist
psql -U postgres -h postgres -p 5432 -c "DROP DATABASE IF EXISTS \"$DB_NAME\";"
psql -U postgres -h postgres -p 5432 -c "DROP DATABASE IF EXISTS \"items-$DB_NAME\";"
psql -U postgres -h postgres -p 5432 -c "DROP DATABASE IF EXISTS \"userprofile-$DB_NAME\";"

# Create Databases
echo "Creating postgres database tables.."

psql -U postgres -h postgres -p 5432 -c "CREATE USER $PSQL_USER WITH PASSWORD '$PSQL_PWD';"
psql -U postgres -h postgres -p 5432 -c "ALTER USER $PSQL_USER WITH SUPERUSER;"
psql -U postgres -h postgres -p 5432 -c "CREATE DATABASE \"$DB_NAME\";"
psql -U postgres -h postgres -p 5432 -c "CREATE DATABASE \"items-$DB_NAME\";"
psql -U postgres -h postgres -p 5432 -c "CREATE DATABASE \"userprofile-$DB_NAME\";"
psql -U postgres -h postgres -p 5432 -c "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME to $PSQL_USER;"
psql -U postgres -h postgres -p 5432 -c "GRANT ALL PRIVILEGES ON DATABASE \"items-$DB_NAME\" to $PSQL_USER;"
psql -U postgres -h postgres -p 5432 -c "GRANT ALL PRIVILEGES ON DATABASE \"userprofile-$DB_NAME\" to $PSQL_USER;"

# Prepare trackList.json file for JBrowse
cp /conf/trackList.json /jbrowse

sed -i "s/PORT/$JBROWSE_PORT/g" /jbrowse/trackList.json
sed -i "s/TAXON_ID/$TAXON_ID/g" /jbrowse/trackList.json
sed -i "s/DB/$DB_NAME/g" /jbrowse/trackList.json
