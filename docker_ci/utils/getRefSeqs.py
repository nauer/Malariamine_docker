#!/usr/bin/env python3
import json
import intermine.webservice
import argparse

parser = argparse.ArgumentParser(description='Create refSeqs.json')
parser.add_argument('uri', type=str,
                    help='Uri to the intermine. For example http://www.flymine.org')
parser.add_argument('taxonId', type=str,
                    help='taxonId of the organism')

args = parser.parse_args()

mine = intermine.webservice.Service("{}".format(args.uri))
refs = mine.model.Chromosome.where("length", ">", 0).where("organism.taxonId", "=", args.taxonId)

with open("refSeqs.json", "w") as f:
    f.write(json.dumps([{"name": r.primaryIdentifier, "start": 0, "end": r.length} for r in refs], indent = 2))
